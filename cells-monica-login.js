{
  const {
    html,
  } = Polymer;
  /**
    `<cells-monica-login>` Description.

    Example:

    ```html
    <cells-monica-login></cells-monica-login>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-monica-login | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsMonicaLogin extends Polymer.Element {

    static get is() {
      return 'cells-monica-login';
    }

    static get properties() {
      return {
        prop1: {
            type: String,
            value: 'Inicio de Sesión.'
          },
          prop2 : String,
          prop3 : String, 
          prop4 : {
            type : Boolean,
            notify: true
          } 
      };
    }
    validarUsuario() {
      if(this.prop2 == 'Monica' && this.prop3 == '12345'){
        this.set('prop4', true);
      }else {
        console.log('error')
      }
    }

    static get template() {
      return html `
      <style include="cells-monica-login-styles cells-monica-login-shared-styles"></style>
      <slot></slot>
          <p>Login Mónica</p>
          <div align="center">
            <h2>[[prop1]]</h2>
                </div>
                <div align="center">
                  <p>Usuario:    <input type="text" name="login" id="login" value="{{prop2::input}}" /></p>
                  <p>Contraseña: <input type="password" name="pass" id="pass" value="{{prop3::input}}" /></p>
                    <label>Nombre de usuario: [[prop2]]</label><br />
                    <label>Contraseña: [[prop3]]</label><br /><br /><br />
                  <button on-click="validarUsuario">Aceptar</button><br />
                  <label>Prueba: [[prop4]]</label><br /><br /><br />  
              </div>
      `;
    }

  }

  customElements.define(CellsMonicaLogin.is, CellsMonicaLogin);
}